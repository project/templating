<?php

namespace Drupal\templating\Form;


use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\Component\Serialization\Yaml;
/**
 * Edit config variable form.
 */
class ConfigTemplateExportImport extends FormBase
{

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'config_template_export_import_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $config_name = '')
    {
        $module_list = \Drupal::service('extension.list.module')->getList();
        $options =[];
        $config_name = "templating.settings" ;
        $config = \Drupal::config($config_name) ;
        $output =  $config->get('location');
        foreach ($module_list as $key_mod => $mod){
            if ($mod->origin !='core' && strpos($mod->subpath, 'modules/contrib') === false && $mod->status == 1) {
              $options[$key_mod] = $mod->getName() ;
            }
        }
        $form['location'] = [
            '#type' => 'select',
            '#title' => $this->t('Module location'),
            '#options' => $options,
            '#description' => 'Make sure to have permission to read the Module config ( PATH_MODULE/config/install )',
            '#required' => TRUE,
            '#default_value' => $output
        ];

        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Export All'),
        ];
        $form['actions']['import'] = [
            '#type' => 'submit',
            '#value' => $this->t('Import All'),
            '#submit' => ['::submitImport'],
        ];

        $form['actions']['cancel'] = array(
            '#type' => 'link',
            '#title' => $this->t('Back to list'),
            '#url' => $this->buildCancelLinkUrl(),
        );

        return $form;
    }
    /**
     * {@inheritdoc} Import
     */
    public function submitImport(array &$form, FormStateInterface $form_state)
    {

            $values = $form_state->getValues();
            if ($values['location']) {
                $config_name = "templating.settings" ;
                $location =  $values['location'];
                $this->configFactory()->getEditable($config_name)
                    ->set('location',$location)
                    ->save();
                if (\Drupal::moduleHandler()->moduleExists($location)) {
                    $module_handler = \Drupal::service('module_handler');
                    $path_module =  $module_handler->getModule($location)->getPath();
                    $fileSystem = \Drupal::service('file_system');
                    if (!is_dir($path_module . '/config/optional')) {
                        $this->messenger()->addError('Config  directory : ' . $path_module . '/config/optional   not exist' );
//                        if ($fileSystem->mkdir($path_module . '/config/install', 0777, TRUE) === FALSE) {
//                            $this->messenger()->addError('Failed to create directory ' . $path_module . '/config/install');
//                        }else{
//                            \Drupal::service('config.installer')->installDefaultConfig('module',    $location);
//                            $this->messenger()->addMessage($this->t('Config importation was successfully'));
//                        }
                    }else{
                        $config_path = $path_module . '/config/optional';
                        $config_source = new \Drupal\Core\Config\FileStorage($config_path);
                        $result_sources = $config_source->listAll() ;
                        $config_storage = \Drupal::service('config.storage');
                        foreach ($result_sources as $key => $config_name) {
                            if($config_storage->exists($config_name)){
                                $status = $config_storage->write($config_name, $config_source->read($config_name));
                                if($status){
                                   $this->messenger()->addMessage($this->t('Config importation '.$config_name.' update successfully '));
                                }
                                else{
                                    $this->messenger()->addError('Failed to Import '.$config_name);
                                }

                            }else{
                                $config_content =$config_source->read($config_name);
                                $status = $this->configFactory()->getEditable($config_name)
                                    ->set('content', $config_content['content'])
                                    ->set('type', $config_content['type'])
                                    ->save();
                                if($status){
                                $this->messenger()->addMessage($this->t('Config importation '.$config_name.' created was successfully '));
                                }else{
                                    $this->messenger()->addError('Failed to Import '.$config_name);
                                }
                            }
                        }

                     //   \Drupal::service('config.installer')->installOptionalConfig($config_source);
                      //  $this->messenger()->addMessage($this->t('Config importation ' . $path_module . '/config/optional was successfully'));
                    }

                }else{
                    $this->messenger()->addError('Failed to Import Templates');
                }

            }
    }

    /**
     * Builds the cancel link url for the form.
     *
     * @return Url
     *   Cancel url
     */
    private function buildCancelLinkUrl()
    {
        $query = $this->getRequest()->query;
        if ($query->has('destination')) {
            $options = UrlHelper::parse($query->get('destination'));
            $url = Url::fromUri('internal:/' . $options['path'], $options);
        } else {
            $url = Url::fromRoute('templating.manager');
        }

        return $url;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {

    }

    /**
     * {@inheritdoc} Export
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        if ($values['location']) {
            $config_name = "templating.settings" ;
            $location =  $values['location'];
            $this->configFactory()->getEditable($config_name)
                ->set('location',$location)
                ->save();
            if (\Drupal::moduleHandler()->moduleExists($location)) {
                $module_handler = \Drupal::service('module_handler');
                $path_module =  $module_handler->getModule($location)->getPath();
                $fileSystem = \Drupal::service('file_system');
                $path_module = DRUPAL_ROOT."/".$path_module;
                if (is_dir($path_module . '/config/optional')) {
                    $results = $this->configFactory()->listAll("template.");
                    foreach ($results as $key => $result) {
                        $config = \Drupal::config($result) ;
                        $data = $config->getOriginal();
                        try {
                            $output = Yaml::encode($data);
                            $file = $path_module.'/config/optional/'.$result.'.yml' ;
                            file_put_contents($file, $output);

                        }
                        catch (InvalidDataTypeException $e) {
                            $this->messenger()->addError($this->t('Invalid data detected for @name : %error', array('@name' => $config_name, '%error' => $e->getMessage())));
                            return;
                        }


                    }
                    $this->messenger()->addMessage($this->t('Config Exportation was successfully'));
                }else{
                        if ($fileSystem->mkdir($path_module . '/config/optional', 0777, TRUE) === FALSE) {
                            $this->messenger()->addError('Failed to create directory ' . $path_module . '/config/optional');
                        }else{
                            $results = $this->configFactory()->listAll("template.");
                            foreach ($results as $key => $result) {
                                $config = \Drupal::config($result) ;
                                $data = $config->getOriginal();
                                try {
                                    $output = Yaml::encode($data);
                                    $file = $path_module.'/config/optional/'.$result.'.yml' ;
                                    file_put_contents($file, $output);
                                }
                                catch (InvalidDataTypeException $e) {
                                    $this->messenger()->addError($this->t('Invalid data detected for @name : %error', array('@name' => $result, '%error' => $e->getMessage())));
                                    return;
                                }


                            }
                            $this->messenger()->addMessage($this->t('Config Exportation was successfully'));
                        }
                }
            }

        }
    }

}
