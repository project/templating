<?php

namespace Drupal\templating\Form;


use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
/**
 * Edit config variable form.
 */
class ConfigTemplateCreate extends FormBase
{

    protected $step = -1;

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'config_template_create_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $config_name = '')
    {
        if($this->step == -1){
            $form['template_index'] = [
                '#type' => 'select',
                '#title' => $this->t('Template'),
                '#options' => [
                    0 =>'Block content',
                    1 => 'Node',
                  //  2 =>'Region',
                  //  3 =>'Custom',
                 //   4 =>'Region',
                //    5 =>'Block'
                ],
                '#required' => True
            ];
        }
        if($this->step == 1){
            $form = TemplatingForm::nodeForm($form);
        }
        if($this->step == 2){
            $form = TemplatingForm::pageForm($form);
        }
        if($this->step == 3){
            $form = TemplatingForm::customForm($form);
        }
        if($this->step == 0){
            $form = TemplatingForm::blockForm($form);
        }
        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Save'),
        ];
        $form['actions']['cancel'] = array(
            '#type' => 'link',
            '#title' => $this->t('Back to Template list'),
            '#url' => $this->buildCancelLinkUrl(),
        );
        return $form;

    }

    /**
     * Builds the cancel link url for the form.
     *
     * @return Url
     *   Cancel url
     */
    private function buildCancelLinkUrl()
    {
        $query = $this->getRequest()->query;
        if ($query->has('destination')) {
            $options = UrlHelper::parse($query->get('destination'));
            $url = Url::fromUri('internal:/' . $options['path'], $options);
        } else {
            $url = Url::fromRoute('templating.manager');
        }

        return $url;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        if($this->step == - 1 ) {
            $form_state->setRebuild();
            $this->step = $values['template_index'] ;
        }else {

            $theme = isset($values["theme"])? $values["theme"] : "" ;
            $config_name = null ;
            $config_name_init = null ;
            // template custom
            if (isset($values['template_custom'])) {
                $configs = TemplatingForm::customFormSubmit($values);
                if(isset($configs['name'])){
                 $config_name = $configs['name'] ;
                 $config_name_init = $configs['type'] ;
                }
            }
            // template block_content
            if (isset($values['blocktype'])) {
                $configs = TemplatingForm::blockFormSubmit($values);
                if(isset($configs['name'])){
                   $config_name = $configs['name'] ;
                   $config_name_init = $configs['type'] ;
                   $bundle = $values['blocktype'] ;
                }
            }
            // template node
            if (isset($values['bundle'])) {
                $configs = TemplatingForm::nodeFormSubmit($values);
                if(isset($configs['name'])){
                $config_name = $configs['name'] ;
                $config_name_init = $configs['type'] ;
                $bundle = $values['bundle'] ;
                }
            }
            // template page
            if (isset($values['route_name'])) {
                $configs = TemplatingForm::pageFormSubmit($values);
                if(isset($configs['name'])){
                    $config_name = $configs['name'] ;
                    $config_name_init = $configs['type'] ;
                }
            }
            // saving section
            if($config_name && $config_name_init){
                if($bundle == null){
                    $bundle = $config_name_init ;
                }
                $names = $this->configFactory()->listAll("template.");
                $services = \Drupal::service('templating.manager');
                $config_name = $services->formatName($config_name);
                if (in_array($config_name, $names) ) {
                    $this->messenger()->addError($this->t('Template name ' . $config_name . ' exist already '));
                } else {
                    $content = TemplatingForm::defaultContent($config_name_init);
                    $this->configFactory()->getEditable($config_name)
                        ->set('content', $content)
                        ->set('type', $config_name_init)
                        ->set('theme', $theme)
                        ->set('bundle', $bundle)
                        ->set('status', true)
                        ->save();
                    $this->messenger()->addMessage($this->t('Template created was successfully'));
                    return new RedirectResponse(Url::fromRoute('templating.editor',['config'=> $config_name])->toString());
                }
            }else{
                $this->messenger()->addError($this->t('Template name ' . $config_name . ' not create '));
            }
        }
    }

}
