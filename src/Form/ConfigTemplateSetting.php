<?php

namespace Drupal\templating\Form;


use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Config\FileStorage;

/**
 * Edit config variable form.
 */
class ConfigTemplateSetting extends FormBase
{

    protected $step = -1;
    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'config_template_settings_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state, $config_name = '')
    {
        $config_settings = \Drupal::config("template_inline.settings") ;
        $services = \Drupal::service('templating.manager');
        $themes = $services->getThemeList();
        $theme_options = [];
        foreach(array_keys($themes) as $theme){
            $theme_options[$theme] = $theme ;
        }
        $form['theme'] = [
            '#type' => 'checkboxes',
            '#title' => t('Theme'),
            '#options' => $theme_options,
            '#multiple' => TRUE,
            '#required' => TRUE,
            '#default_value' =>  ($config_settings->get('theme'))? $config_settings->get('theme'): [] ,
        ];
        $form['enabled'] = array(
            '#type' => 'checkbox',
            '#title' => t('Enabled template inline'),
            '#default_value' =>  ($config_settings->get('enabled'))? $config_settings->get('enabled'): false 
        );
        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Save'),
        ];

       $form['actions']['config_process'] = [
             '#type' => 'submit',
             '#value' => 'Import Existing Templates',
             '#submit' => [[$this, 'configProcess']],
       ];
        $form['actions']['cancel'] = array(
            '#type' => 'link',
            '#title' => $this->t('Back to Template list'),
            '#url' => $this->buildCancelLinkUrl(),
        );
        var_dump($this->step);
        return $form;

    }
    public function configProcess(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        $theme_names = $values['theme'];
        $items =[];
        foreach ($theme_names as $theme){
            if(!is_numeric($theme)){
                $theme_path = \Drupal::service('extension.list.theme')->getPath($theme);
                $directory = DRUPAL_ROOT.'/'.$theme_path.'/templates/block' ;
                $item_current =  $this->readDirectory($directory,'twig');
                $items = array_merge($item_current,$items);
            }
        }
        foreach ($items as $file){

        }
        $form_state->setRebuild();
        $this->step = $items ;
      //  $theme_path = \Drupal::service('extension.list.theme')->getPath($theme_name);
       // $directory = DRUPAL_ROOT.'/'.$theme_path.'/templates/' ;
       // $results = $this->readDirectory($directory,'twig');
       // kint($results);die();


        $config_path = drupal_get_path('module', 'templating') . '/config';
        $config_path = DRUPAL_ROOT ."/".$config_path;
        $results_prior = $this->readDirectory($config_path.'/base','yml');
        $results_op = $this->readDirectory($config_path.'/optional','yml');
        foreach ($results_prior as $key => $result){
                $config_name =  basename($result,".yml"); 
                if(!$this->isExistDatabase($config_name)){
                  //  $this->importConfig($result);
                }
        }   
        // foreach ($results_op as $key => $result){
        //     $config_name =  basename($result,".yml"); 
        //     if(!$this->isExistDatabase($config_name)){
        //         $this->importConfig($result);
        //     }
        // }   
    }
    public function importConfig($file_path){

        $pathinfo = pathinfo($file_path);
        $config_name = $pathinfo['filename'];
        $path = $pathinfo['dirname'];
        $source = new FileStorage($path);
        $config_storage = \Drupal::service('config.storage');
        if($source->exists($config_name)){
            $config_data = $source->read($config_name);
            $config_info = $this->getInfoConfigYaml($path.'/'.$config_name.'.yml');
            if($config_info['type'] && $config_info['idkey']){
                /** @var \Drupal\Core\Config\Entity\ConfigEntityStorageInterface $entity_storage */
                $entity_storage = \Drupal::entityTypeManager()->getStorage($config_info['type']);
                $entity = $entity_storage->createFromStorageRecord($config_data);
                return $entity->save();
            } else {
                return \Drupal::configFactory()->getEditable($config_name)->setData($config_data)->save();
            }
       }else{
            \Drupal::messenger()->addError('File yml '.$config_name.' not exit in folder '.$path);
            return false;
       }
    }
    public function isExistDatabase($config_name){
        $config_storage = \Drupal::service('config.storage');
        return $config_storage->exists($config_name);
    }
    public function getInfoConfigYaml($file){

        $config_manager = \Drupal::service('config.manager');
        $type = $config_manager->getEntityTypeIdByName(basename($file));
        $entity_manager = $config_manager->getEntityManager();
        $id_key = null;
        if($type){
        $definition = $entity_manager->getDefinition($type);
        $id_key = $definition->getKey('id');
        }
        return [ "idkey"=>$id_key , "type" => $type ];
    }
    public  function readDirectory($directory,$format = 'json')
    {
        $path_file = [];
        if (is_dir($directory)) {
            $it = scandir($directory);
            if (!empty($it)) {
                foreach ($it as $fileinfo) {
                    $element =  $directory . "/" . $fileinfo;
                    if (is_dir($element) && substr($fileinfo, 0, strlen('.')) !== '.') {
                        $childs = $this->readDirectory($element,$format);
                        $path_file = array_merge($childs , $path_file);
                    }else{
                        if ($fileinfo && strpos($fileinfo, '.'.$format) !== FALSE) {
                            if (file_exists($element)) {
                                $path_file[] =  $directory . "/" . $fileinfo;
                            }
                        }
                    }
                }
            }
        }else{
            drupal_set_message(t('No permission to read directory ' . $directory), 'error');
            @chmod($directory  , 0777);
        }
        return $path_file;
    }
    /**
     * Builds the cancel link url for the form.
     *
     * @return Url
     *   Cancel url
     */
    private function buildCancelLinkUrl()
    {
        $query = $this->getRequest()->query;
        if ($query->has('destination')) {
            $options = UrlHelper::parse($query->get('destination'));
            $url = Url::fromUri('internal:/' . $options['path'], $options);
        } else {
            $url = Url::fromRoute('templating.manager');
        }

        return $url;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
    }
 
    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
            // saving section

        $this->configFactory()->getEditable('template_inline.settings')
                        ->set('enabled', $values['enabled'])
                        ->set('theme', $values['theme'])
                        ->save();
    }

}
