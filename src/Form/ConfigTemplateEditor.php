<?php

namespace Drupal\templating\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Edit config variable form.
 */

class ConfigTemplateEditor extends FormBase
{

    /**
     * {
    @inheritdoc}
     */

    public function getFormId()
    {
        return 'config_template_edit_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state, $config_name = '')
    {
        $services = \Drupal::service('templating.manager');
        $query = $this->getRequest()->query;
        $config_name = $query->get('config');
        $config = \Drupal::config($config_name);
        $html = $config->get('content');
        $status = ($config->get('status')) ? $config->get('status') : false;
        $css = ($config->get('css')) ? $config->get('css') : '';
        $js = ($config->get('js')) ? $config->get('js') : '';
        $name = $services->removePrefix($config_name);
        $name = ($config->get('name')) ? $config->get('name') : $name;
        $label = ($config->get('label')) ? $config->get('label') : '';

        $form['help'] = [
            '#type' => 'item',
            '#markup' => 'EDIT BLOCK CONTENT '.$name,
        ];
        $form['label'] = [
            '#type' => 'textfield',
            '#title' => t('Label'),               
            '#default_value' => $label
        ];
        $form['tabs'] = array(
            '#type' => 'horizontal_tabs',
            '#tree' => true,
            '#prefix' => '<div id="unique-wrapper">',
            '#suffix' => '</div>',
        );
        $form['tabs']['content_html'] = array(
            '#type' => 'details',
            '#title' => t('HTML'),
            '#collapsible' => true,
            '#collapsed' => true,
        );
        $form['tabs']['content_css'] = array(
            '#type' => 'details',
            '#title' => t('CSS'),
            '#collapsible' => true,
            '#collapsed' => true,
        );
        $form['tabs']['content_js'] = array(
            '#type' => 'details',
            '#title' => t('JS'),
            '#collapsible' => true,
            '#collapsed' => true,
        );
        $form['tabs']['content_preview'] = array(
            '#type' => 'details',
            '#title' => t('Run'),
            '#collapsible' => true,
            '#collapsed' => true,
        );

        $form['tabs']['content_guide_twig'] = array(
            '#type' => 'details',
            '#title' => t('Help'),
            '#collapsible' => true,
            '#collapsed' => true,
        );
        $status_acer = \Drupal::moduleHandler()->moduleExists('ace_editor');
        if ($status_acer) {
            $form['tabs']['content_html']['html'] = [
                '#type' => 'text_format',
                '#format' => 'html_code',
                '#default_value' => $html,
            ];
            $form['tabs']['content_css']['css'] = [
                '#type' => 'text_format',
                '#format' => 'css_code',
                '#default_value' => $css,
            ];
            $form['tabs']['content_js']['js'] = [
                '#type' => 'text_format',
                '#format' => 'javascript_code',
                '#default_value' => $js,
            ];
            $form['tabs']['content_guide_twig']['twig'] = [
                '#type' => 'item',
                '#markup' => 'guide twig',
            ];
            $form['tabs']['content_preview']['preview'] = [
                '#type' => 'item',
                '#markup' => 'preview',
            ];
        }
        $form['#attached']['library'][] = 'field_group/formatter.horizontal_tabs';

        $form['config_name'] = array(
            '#type' => 'hidden',
            '#default_value' => $name,
        );
        $form['status'] = array(
            '#type' => 'checkbox',
            '#title' => t('Enable template inline'),
            '#default_value' => $status,
        );
        $form['actions'] = ['#type' => 'actions'];
        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Save Template'),
        ];
        $form['actions']['preview_display'] = [
            '#type' => 'link',
            '#title' => $this->t(' Save and preview'),
            '#options' => [
                'attributes' => ['target' => '_blank'],
            ],
            '#url' => $this->buildPreview(),
            '#attributes' => ['class' => ['button js-form-submit form-submit']],

        ];
        $form['actions']['cancel'] = array(
            '#type' => 'link',
            '#title' => $this->t('Back to Template list'),
            '#url' => $this->buildCancelLinkUrl(),
        );

        return $form;
    }

    public function revertProcess(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        if (isset($values['config_name'])) {
            $config_name = $values['config_name'];
            $config = \Drupal::config($config_name);
            $theme_name = $config->get('theme');
            $theme = \Drupal::service('extension.list.theme')->getPath($theme_name);
            $services = \Drupal::service('templating.manager');
            $name = $services->removePrefix($config_name);
            $element = DRUPAL_ROOT . '/' . $theme . '/templates/templating/' . $name;
            if (file_exists($element)) {
                $content = file_get_contents($element, FILE_USE_INCLUDE_PATH);
                if ($content) {
                    $this->configFactory()->getEditable($config_name)
                        ->set('content', $content)
                        ->set('status', $values['status'])
                        ->save();
                    $this->messenger()->addMessage($this->t('Template ' . $name . ' imported  was successfully'));
                } else {
                    $this->messenger()->addMessage($this->t('Template ' . $name . ' have content Error '), 'error');
                }
            } else {
                $this->messenger()->addMessage($this->t('Template ' . $name . ' not  exit in theme : ' . $theme_name), 'error');
            }

        }
    }
    /**
     * {
    @inheritdoc}
     */

    public function validateForm(array &$form, FormStateInterface $form_state)
    {
    }

    /**
     * {
    @inheritdoc}
     */

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $query = $this->getRequest()->query;
        $config_name = $query->get('config');
        $values = $form_state->getValues();
        if ($config_name && $values['tabs']['content_html']['html']['value'] != '') {
            $content = $values['tabs']['content_html']['html']['value'];
            $css = $values['tabs']['content_css']['css']['value'];
            $js = $values['tabs']['content_js']['js']['value'];
            $name = $values['config_name'];
            $label = $values['label'];
            $this->configFactory()->getEditable($config_name)
                ->set('content', $content)
                ->set('css', $css)
                ->set('label', $label)
                ->set('js', $js)
                ->set('name', $name)
                ->set('status', $values['status'])
                ->save();
            $this->messenger()->addMessage($this->t('Config ' . $config_name . ' update  was successfully'));
        }

    }

    private function buildPreview()
    {
        $query = $this->getRequest()->query;
        $config_name = $query->get('config');
        $config = \Drupal::config($config_name);
        $bundle = $config->get('bundle');
        $url = Url::fromRoute('preview_entity.display',['bundle' => $bundle]);
        return $url;

    }
    /**
     * Builds the cancel link url for the form.
     *
     * @return Url
     *   Cancel url
     */

    private function buildCancelLinkUrl()
    {
        $query = $this->getRequest()->query;

        if ($query->has('destination')) {
            $options = UrlHelper::parse($query->get('destination'));
            $url = Url::fromUri('internal:/' . $options['path'], $options);
        } else {
            $url = Url::fromRoute('templating.manager');
        }

        return $url;
    }

}
