<?php

namespace Drupal\templating\Form;


/**
 * Class TemplatingForm.
 */
class TemplatingForm {
    public  static function nodeForm($form){
        $services = \Drupal::service('templating.manager');
        $themes = $services->getThemeList();
        $theme_options = [];
        foreach(array_keys($themes) as $theme){
            $theme_options[$theme] = $theme ;
        }
        $form['theme'] = [
            '#type' => 'select',
            '#title' => t('Theme'),
            '#options' => $theme_options,
            '#required' => TRUE
        ];
        $bundle_list_name = \Drupal::service('entity_type.bundle.info')->getBundleInfo('node');
        $bundle_options = [];
        foreach(array_keys($bundle_list_name) as $type){
            $bundle_options[$type] = $type ;
        }
        $form['bundle'] = [
            '#type' => 'select',
            '#title' => t('Content Type'),
            '#options' => $bundle_options,
            '#required' => TRUE
        ];
        $mode_views = $services::getModeViewList('node');
        $form['mode_view'] = [
            '#type' => 'select',
            '#title' => t('Mode view '),
            '#options' => $mode_views,
            '#required' => TRUE
        ];
        $form['nid'] = [
            '#type' => 'textfield',
            '#title' => t('NID'),
            '#description' => t('Leave empty , if you want to apply for all node'),
            '#default_value' => ''
        ];
        return $form ;

    }
    public static  function nodeFormSubmit($values){
        $config_name = null ;
        if (isset($values['bundle'])
            && isset($values['mode_view']) &&
            isset($values['theme'])) {
            if($values['nid'] !=""){
                $node = \Drupal::entityTypeManager()->getStorage('node')->load(trim($values['nid']));
                if (is_object($node)) {
                    $type = $node->bundle();
                    $config_name = "template.node--".$values['theme']."-". trim($type) . "-" .trim($values['nid'])."-".trim($values['mode_view']).".html.twig";
                }
            }
            else {
                $config_name = "template.node--".$values['theme'].'-'.trim($values['bundle']).'-'.trim($values['mode_view']).'.html.twig';
            }
        }
        return [
            "name" => $config_name ,
            "type" => "node"
        ];
    }
    public static  function pageForm($form){
        $route_list = [];
        $routes = \Drupal::service('router.route_provider')->getAllRoutes();
        foreach($routes as $key => $route){
            $route_list[$key] = $route->getPath();
        }
      
           $services = \Drupal::service('templating.manager');
           $themes = $services->getThemeList();
           $region_list = $services->getRegionList();
           $theme_options = [];
           foreach(array_keys($themes) as $theme){
               $theme_options[$theme] = $theme ;
           }

        $form['theme'] = [
            '#type' => 'select',
            '#title' => t('Themes'),
            '#options' => $theme_options,
            '#required' => TRUE
        ];
        $form['route_name'] = [
            '#type' => 'select',
            '#title' => t('Page path'),
            '#options' => $route_list,
            '#required' => TRUE,
        ];
        $form['region'] = [
            '#type' => 'select',
            '#title' => t('Region name'),
            '#options' => $region_list,
            '#required' => TRUE,
        ];
        return $form ;
    }
    public static  function pageFormSubmit($values){
        $config_name = null ;
        if (isset($values['route_name']) &&
            isset($values['theme'])) {
              $config_name = "template.page--".$values['theme']."-".trim($values['route_name'])."-".trim($values['region']).".html.twig";
        }
        return [
            "name" => $config_name ,
            "type" => "page"
        ];
    }
    public static  function blockForm($form){
         $block_type_lists = \Drupal::service('entity.manager')->getStorage('block_content_type')->loadMultiple();
         $block_type = ['none' => 'None'];
         foreach ($block_type_lists as $key => $type) {
             $block_type[$key] = $type->label();
         }
         $block_type_lists = \Drupal::service('entity.manager')->getStorage('block')->loadMultiple();
         $list = [];
         foreach ($block_type_lists as $key => $item) {
             $themes[$item->getTheme()] = $item->getTheme();
             $list[] = $item->getPluginId();
         }
         $defaultThemeName = \Drupal::config('system.theme')->get('default');
         $form['theme'] = [
             '#type' => 'select',
             '#title' => t('Themes'),
             '#options' => $themes,
             '#required' => FALSE,
             '#default_value' => $defaultThemeName
         ];
         $form['blocktype'] = [
             '#type' => 'select',
             '#title' => t('Block Custom Type'),
             '#options' => $block_type,
             '#required' => FALSE
         ];
         $form['blockid'] = [
             '#type' => 'textfield',
             '#title' => t('Plugin ID or Block ID'),
             '#description' => t('Leave empty , if you want to apply for all block'),
         ];


         return $form ;
     }
    public static  function blockFormSubmit($values){
        $config_name = null ;
        if (isset($values['blocktype'])
            && isset($values['blockid']) &&
            isset($values['theme'])) {
            if($values['blockid'] !=""){
                $block_custom = \Drupal::entityTypeManager()->getStorage('block_content')->load(trim($values['blockid']));
                if (is_object($block_custom)) {
                    $block_type = $block_custom->bundle();
                    $config_name = "template.block--".$values['theme']."-". trim($block_type) . "-" .trim($values['blockid'])."-full.html.twig";
                }
            }
            else {
                $config_name = "template.block--".$values['theme'].'-'.trim($values['blocktype'].'-full.html.twig');
            }
        }
        return [
            "name" => $config_name ,
            "type" => "block_content"
        ];
    }
    public static function customForm($form){
        $services = \Drupal::service('templating.manager');
        $themes = $services->getThemeList();
        $form['theme'] = [
              '#type' => 'select',
              '#title' => t('Theme'),
              '#options' => array_keys($themes),
              '#required' => TRUE
        ];
        $form['template_custom'] = [
             '#type' => 'textfield',
             '#title' => t('Template name'),
             '#description' => t('For example : node.platforme_theme.demande.full'),
        ];

         return $form ;
     }
    public static  function customFormSubmit($values){
        return [
            "name" => "template.".$values['template_custom'] ,
            "type" => "custom"
        ];
    }
    public static function defaultContent($type = 'default'){
        $template = '<div class="templating">{{ content }}</div>';
        if($type =='page'){
            $template = '<div class="templating">{{ content }}</div>';
        }
        return $template;
    }

}
