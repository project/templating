<?php

namespace Drupal\templating\Form;

use Drupal\Core\Url;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
/**
 * Class ContentExportSettingForm.
 */
class ConfigTemplateManagerForm extends FormBase {

//  /**
//   * {@inheritdoc}
//   */
  protected function getEditableConfigNames() {
    return [
      'templating.source',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'templating_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
      $services = \Drupal::service('templating.manager');
      $params = $this->getRequest()->request->all();
      $query = $this->getRequest()->query;

      if($query->has('delete')){
          $delete = $query->get('delete');
              \Drupal::configFactory()->getEditable($delete)->delete();
          $this->messenger()->addMessage($this->t('Config '.$delete.' delete was successfully'));

      }
      $header = [
          'id' =>  t('ID'),
          'label' => t('Label'),
          'config_name' => t('Config name'),
          'status' => t('Status'),
          'operation' => array('data' => $this->t('Operations'))
      ];
      $output = [];
      $results = $this->configFactory()->listAll("template.");
      foreach ($results as $key => $result) {
          $output = $services->getResults($output,$key,$result,$params);
      }

      $form['key'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Template Search'),
          '#default_value' => isset($params['key']) ? $params['key'] : '',
      ];
      $form['actions'] = ['#type' => 'actions'];
      $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Search'),
      ];
      $form['actions']['enable'] = [
          '#type' => 'submit',
          '#value' => 'Enable',
          '#submit' => [[$this, 'enableProcess']],
      ];
      $form['actions']['disable'] = [
          '#type' => 'submit',
          '#value' => 'Disable',
          '#submit' => [[$this, 'disableProcess']],
      ];

      $form['table'] = array(
          '#type' => 'tableselect',
          '#weight' => 999,
          '#header' => $header,
          '#options' => $output,
          '#empty' => $this->t('No content available.')
      );

    return $form ;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

      $form_state->disableRedirect();
  }
    public function disableProcess(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        $services = \Drupal::service('templating.manager');
        if($values['table']){
            $list = $values['table'];
            foreach ($list as $key => $item){
                if(!is_numeric($item)){
                    $status = $services->saveTemplate($item,['status'=> 0 ]);
                    if($status){
                        $message = t('Template ' .$item. ' disable successfully');
                        \Drupal::messenger()->addMessage($message);
                    }
                }
            }
        }
    }
    public function enableProcess(array &$form, FormStateInterface $form_state)
    {
        $values = $form_state->getValues();
        $services = \Drupal::service('templating.manager');
        if($values['table']){
            $list = $values['table'];
            foreach ($list as $key => $item){
                if(!is_numeric($item)){
                    $status = $services->saveTemplate($item,['status'=> 1 ]);
                    if($status){
                        $message = t('Template ' .$item. ' enable successfully');
                        \Drupal::messenger()->addMessage($message);
                    }
                }
            }
        }
    }
    public function deployProcess(array &$form, FormStateInterface $form_state)
    {
        $batch = [
            'title' => $this->t('Deploy Template ...'),
            'operations' => [],
            'init_message' => $this->t('Starting ..'),
            'progress_message' => $this->t('Processd @current out of @total.'),
            'error_message' => $this->t('An error occurred during processing.'),
            'finished' => 'Drupal\templating\Form\ConfigTemplateManagerForm::deployFinishedCallback',
        ];
        $values = $form_state->getValues();
        $services = \Drupal::service('templating.manager');
        if($values['table']){
            $list = $values['table'];
             kint(  $list);
            foreach ($list as $key => $item){
                if(!is_numeric($item)){
                  //  $batch['operations'][] = [$services->deploy($item), []];
                }
            }
          ///  batch_set($batch);

        }
        die();

    }
    public static function deployFinishedCallback($success, $results, $operations) {
        if ($success) {
            $message = t('Template deployement successfully');
            \Drupal::messenger()->addMessage($message);
        }
        return new RedirectResponse(Url::fromRoute('templating.manager')->toString());
    }
}
