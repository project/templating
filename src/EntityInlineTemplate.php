<?php
namespace Drupal\templating;

use Drupal\Core\Url;
use Drupal\Core\Render\Markup;

class EntityInlineTemplate
{
    public function getRegionList(){
        $result = [];
        $config_settings = \Drupal::config("template_inline.settings") ;
        $allowed_theme = $config_settings->get('theme');
        $allowed_theme = array_values($allowed_theme);
        foreach ($allowed_theme as $theme){
            if(!is_numeric($theme)){
              $system_region = system_region_list($theme, $show = REGIONS_ALL);
              foreach ($system_region as $key => $region){
                    $result[$key] = $key ;
              }
            }
        }
        return $result ;
    }
    public  static  function  getModeViewList($entity_name){
        $mode_view_list = [] ;
        $mode_views = \Drupal::entityTypeManager()->getStorage('entity_view_mode')->loadMultiple();
        foreach ($mode_views as $key => $item){
            $type = $item->getTargetType();
            if($type == $entity_name){
                $mode_view_list[str_replace($type.'.', '', $key)] = str_replace($type.'.', '', $key);
            }
        }
        return $mode_view_list ;
    }
     /**
      *  template.[ENTITY_NAME].[THEME].[BUNDLE].[MODE_VIEW]
     **/
     function getTemplateBlock($variables){
         $plugin_id = trim($variables['elements']['#plugin_id']);
         $base_plugin_id = trim($variables['elements']['#base_plugin_id']);
         $theme = $this->is_allowed();
         if(!$theme){
             return false;
         }

         $suggestion_1 = $this->formatName('template.block--'.$theme.'-'.$plugin_id.'.html.twig');
         $suggestion_2 = $this->formatName('template.block--'.$theme.'-'.$base_plugin_id.'.html.twig');
         $output = false ;
         $config = \Drupal::config($suggestion_2) ;
         $config_id = \Drupal::config($suggestion_1) ;
         if($config_id && $config_id->get('content')){
             $config = $config_id;
         }
         if($config && $config->get('content') &&
             $config->get('status')
         ){
             $output =  $config->get('content');
         }
         return $output ;
     }
     function formatName($name){
         return str_replace('_', '-', $name);
     }
    function removePrefix($name){
        return str_replace('template.', '', $name);
    }
    function generateTemplateLinkBlockContent($block,$mode_view){
        $activeThemeName = \Drupal::service('theme.manager')->getActiveTheme();
        $theme = $activeThemeName->getName();
        $bundle = $block->bundle();
        $id = $block->id();
        $suggestion = $this->formatName('template.block--'.$theme.'-'.$bundle."-".$mode_view.".html.twig");
        $suggestion_id = $this->formatName('template.block--'.$theme.'-'.$bundle."-".$mode_view."-".$id.".html.twig");
        $config = \Drupal::config($suggestion) ;
        $config_id = \Drupal::config($suggestion_id) ;
        if($config_id && $config_id->get('content')){
            $config = $config_id;
            $suggestion = $suggestion_id ;
        }
        if($config  && $config->get('content')){
           return $suggestion ;
        }
        return false ;
    }
     function getTemplateBlockContent($build, $content_block, $display){
     
        $theme = $this->is_allowed();
        if(!$theme){
            return false;
        }
         $mode_view = $build['#view_mode'];
         $bundle = $content_block->bundle();
         $id = $content_block->id();
         $suggestion = $this->formatName('template.block--'.$theme.'-'.$bundle."-".$mode_view.".html.twig");
         $suggestion_id = $this->formatName('template.block--'.$theme.'-'.$bundle."-".$mode_view."-".$id.".html.twig");
         $output = false ;
         $config = \Drupal::config($suggestion) ;
         $config_id = \Drupal::config($suggestion_id) ;
         if($config_id && $config_id->get('content')){
             $config = $config_id;
         }
         if($config && $config->get('content') &&
             $config->get('status')
         ){
             $output =  $config->get('content');
         }
         return $output ;
     }
     function is_allowed(){
        $activeThemeName = \Drupal::service('theme.manager')->getActiveTheme();
        $theme = $activeThemeName->getName();
        $config_settings = \Drupal::config("template_inline.settings") ;
        $enabled = false ;
        if($config_settings->get('theme')){
            $allowed_theme = $config_settings->get('theme');
            $allowed_theme = array_values($allowed_theme);
            $allowed_theme = array_map('strval',  $allowed_theme);
            $enabled = $config_settings->get('enabled');
        }else{
             \Drupal::messenger()->addError('Please select theme in template settings /admin/config/template/settings');
        }
        if(!$enabled){
            return false ;
        }
        if(!in_array($theme , $allowed_theme) ){
           return false ;
        }
        return $theme ;
     }
     function getTemplatePage(&$variables){
        $theme = $this->is_allowed();
        if(!$theme){
            return false;
        }
        $current_route = \Drupal::routeMatch();
        $route_name = $current_route->getRouteName();   
        $entity_name = $variables['page']['#type'];
        foreach($variables['page'] as $key => $region){
            if(!$this->str_starts_with($key, '#')) {
                $suggestion_1 = $this->formatName("template.".$entity_name."__".$theme."_".$route_name."_".$key.".html.twig") ;
                $output = false ;
                $config = \Drupal::config($suggestion_1) ;
                if($config && $config->get('content') &&
                    $config->get('status')
                ){
                    $output =  $config->get('content');
                }
                if($output){
                  $element = [
                    '#type' => 'inline_template',
                    '#template' => $output,
                    '#context' => [
                        'content' =>  $variables['page'][$key]
                    ]
                  ];
                  $variables['page'][$key] = $element;
                }
            }
        }
     }
     function str_starts_with($string, $query){
      return  (substr($string, 0, strlen($query)) === $query) ;
     }
     function getTemplateNode($build, $entity, $display){
        $theme = $this->is_allowed();
        if(!$theme){
            return false;
        }
        
         $entity_name = 'node';
         $view_mode = $build['#view_mode'] ;
         $bundle = $entity->bundle();
         $id = $entity->id();
         
        $output = false ;
        $suggestion_1 = $this->formatName("template.".$entity_name."__".$theme."_".$bundle."_".$view_mode.".html.twig") ;
        $suggestion_2 = $this->formatName("template.".$entity_name."__".$theme."_".$bundle."_".$view_mode."_".$id.".html.twig") ;

        $config = \Drupal::config($suggestion_1) ;
        $config_id = \Drupal::config($suggestion_2) ;
        if($config_id && $config_id->get('content')){
             $config = $config_id;
        }
        if($config && $config->get('content') &&
            $config->get('status')
        ){
            $output =  $config->get('content');
        }
        return $output ;
    }
    function getTemplateEntity($variables){
        $theme = $this->is_allowed();
        if(!$theme){
            return false;
        }
        
         $entity_name = $variables['element']['#entity_type'];
         $entity = $variables['element']['#'.$entity_name] ;
         $view_mode = $variables['element']['#view_mode'] ;
         $bundle = $entity->bundle();
         $id = $entity->id();
         
        $output = false ;
        $suggestion_1 = $this->formatName("template.".$entity_name."__".$theme."_".$bundle."_".$view_mode.".html.twig") ;
        $suggestion_2 = $this->formatName("template.".$entity_name."__".$theme."_".$bundle."_".$view_mode."_".$id.".html.twig") ;

        $config = \Drupal::config($suggestion_1) ;
        $config_id = \Drupal::config($suggestion_2) ;
        if($config_id && $config_id->get('content')){
             $config = $config_id;
        }
        if($config && $config->get('content') &&
            $config->get('status')
        ){
            $output =  $config->get('content');
        }
        return $output ;
    }
     function getResults($output,$key,$result,$params){
         $operations = [] ;
         $config = \Drupal::config($result) ;
         $status =  $config->get('status');
         $label =  ($config->get('label'))?$config->get('label'):' ';
         $theme =  $config->get('theme');
         $bundle =  $config->get('bundle');
         $operations['edit'] = array(
             'title' => t('Edit'),
             'url' => Url::fromRoute('templating.editor', array('config'=>$result))
         );
//       $operations['updateFile'] = array(
//              'title' => $this->t('update File Yml'),
//              'url' => Url::fromRoute('templating.manager', array('updateFile' => $result))
//        );
         $operations['remove'] = array(
             'title' => t('Remove'),
             'url' => Url::fromRoute('templating.manager', array('delete' => $result))
         );
//          $operations['removeFile'] = array(
//              'title' => $this->t('Remove Yml'),
//              'url' => Url::fromRoute('templating.manager', array('deleteFile' => $result))
//          );
         if(isset($params['key']) && $params['key'] !=""){
             if (is_string($params['key']) && strpos($result, $params['key']) !== false) {
                 $output[$result] = [
                     'id' => $key+1 ,
                     'label' =>   $label ,
                     'config_name' =>  $this->renderName($result),
                     'status' =>  ($status == 1) ? 'Enable':'Disable',
                     'operation' => array('data' => array('#type' => 'operations', '#links' => $operations)),
                 ];
             }
         }else{
             $output[$result] = [
                 'id' => $key+1 ,
                 'label' =>   $label ,
                 'config_name' =>  $this->renderName($result),
                 'status' =>  ($status == 1) ? 'Enable':'Disable',
                 'operation' => array('data' => array('#type' => 'operations', '#links' => $operations)),
             ];
         }
         return $output ;

     }
     function getThemeList(){
         $list = \Drupal::service('extension.list.theme')->getList();
         $themes = [];
         foreach ($list as $theme){
             if(is_object($theme)){
                 $path = $theme->getPath();
                 $path_array = explode('/',$path);
                 if(!empty($path_array)
                     &&$path_array[0]
                     && $path_array[1]
                     && $path_array[0] =='themes' && $path_array[1] =='custom'
                     && $theme->status == 1
                 ){
                     $themes[$theme->getName()] = [
                         "name"=>$theme->getName(),
                         "path"=> $theme->getPath()
                     ];
                 }
             }
         }
         return $themes ;

     }
     function getThemePath($item){
         $config = \Drupal::config($item) ;
         $theme_name = $config->get('theme') ;
         $list = \Drupal::service('extension.list.theme')->getList();
         if(in_array($theme_name,$list)){
             return \Drupal::service('extension.list.theme')->getPath($theme_name);
         }else{
             return false ;
         }

     }
     function deploy($item){
         $status = false ;
         $config = \Drupal::config($item) ;
         $theme_name = $config->get('theme') ;
         $content = $config->get('content') ;
         $theme = \Drupal::service('extension.list.theme')->getPath($theme_name);
         $isAllowed = $this->isAllowed($theme);
         if($isAllowed){
             $path_location = DRUPAL_ROOT."/".$theme."/templates/templating";
             $file_name = $this->removePrefix($item);
             $status = $this->generateFile($path_location,$file_name,$content) ;
         }else{
             \Drupal::messenger()->addMessage(t('Failed to deploy template in theme ' . $theme), 'error');
         }
         $name = $this->removePrefix($item);
         if($status){
             \Drupal::messenger()->addMessage(t('Sucess to deploy template  ' . $name), 'status');
         }else{
             \Drupal::messenger()->addMessage(t('Failed to deploy template  ' . $name), 'error');
         }
         return false ;
     }
    function isAllowed($path_theme){
        $path_array = explode('/',$path_theme);
        if(!empty($path_array)
            &&$path_array[0]
            && $path_array[1]
            && $path_array[0] =='themes' && $path_array[1] =='custom'
        ){
            return true ;
        }else{
            return false ;
        }


    }
    function generateFile($directory, $filename, $content)
    {
        $fileSystem = \Drupal::service('file_system');
        if (!is_dir($directory)) {
            if ($fileSystem->mkdir($directory, 0777, TRUE) === FALSE) {
                \Drupal::messenger()->addMessage(t('Failed to create directory ' . $directory), 'error');
                return FALSE;
            }
        }
        if (file_put_contents($directory . '/' . $filename , $content) === FALSE) {
            \Drupal::messenger()->addMessage(t('Failed to write file ' . $filename), 'error');
            return FALSE;
        }
        if (@chmod($directory . '/' . $filename , 0777)) {
         //   \Drupal::messenger()->addMessage(t('Failed to change permission file ' . $filename), 'error');
        }
        return TRUE;
    }
    function renderName($config_name){
        $name = $this->removePrefix($config_name);
        $new = Markup::create( $name.' ( <span style="color:red"> new </span> )');
        $diff = Markup::create($name.' ( <a href="#"><span style="color:blue"> exist </span></a> ) ');
        return  ($this->isExistLocal($config_name))? $diff : $new ;
    }
    public function isExistLocal($config_name){
        $path = $this->getThemePath($config_name);
        $name = $this->removePrefix($config_name);
        if($path){
            $file_path = $this->searchFileInDirectory($name,DRUPAL_ROOT ."/".$path."/templates");
            if(empty($file_path)){
                return false ;
            }else{
                return end($file_path);
            }
        }
        return false ;
    }
    public function saveTemplate($config_name,$fields){
        $config = \Drupal::configFactory()->getEditable($config_name);
        if(isset($fields['content'])){
            $config->set('content', $fields['content']);
        }
        if(isset($fields['css'])){
            $config->set('css', $fields['css']);
        }
        if(isset($fields['label'])){
            $config->set('label', $fields['label']);
        }
        if(isset($fields['js'])){
            $config->set('js', $fields['js']);
        }
        if(isset($fields['name'])){
            $config->set('name', $fields['name']);
        }
        if(isset($fields['status'])){
            $config->set('status', $fields['status']);
        }
        return $config->save();
    }
    public  function searchFileInDirectory($key,$directory)
    {
        $path_file = [];
        if (is_dir($directory)) {
            $it = scandir($directory);
            if (!empty($it)) {
                foreach ($it as $fileinfo) {
                    $element =  $directory . "/" . $fileinfo;
                    if (is_dir( $element ) && substr($fileinfo, 0, strlen('.')) !== '.') {
                        $childs = $this->searchFileInDirectory($key,$element);
                        $path_file = array_merge($childs , $path_file);
                    }else{
                        if ($fileinfo && basename($fileinfo) == $key) {
                            if (file_exists($element)) {
                                $path_file[$key] = $element ;
                            }
                        }
                    }
                }
            }
        }else{
            \Drupal::messenger()->addMessage(t('No permission to read directory ' . $directory ), 'error');
            @chmod($directory  , 0777);
        }
        return $path_file;
    }

}