<?php
/**
 * @file
 * Contains \Drupal\preview_entity\Form\PreviewEntityForm.
 */

namespace Drupal\preview_entity\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Configure search settings for this site.
 */
class PreviewEntityForm extends FormBase {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * Constructs the StaticPageSettingsForm object.
   *
   * @param \Drupal\Core\Entity\EntityManagerInterface $entity_manager
   *   The entity manager.
   */
  public function __construct(EntityManagerInterface $entity_manager) {
    $this->entityManager = $entity_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'preview_entity_form';
  }



  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
   // drupal_flush_all_caches();
    \Drupal::service('page_cache_kill_switch')->trigger();
  $param = $this->get_parameter();
  //  $options['block_content'] = t('Block content');
  //  $options['node'] = t('Node');
  //  $form['entity_name'] = [
  //    '#type' => 'select',
  //    '#title' => t('Block Type'),
  //    '#options' => $options
  //  ];
   $query = \Drupal::entityQuery('block_content_type');
   $options  = $query->execute();
   $options = array_merge(['--none--'],$options );
   $form['bundle'] = [
     '#type' => 'select',
     '#title' => t('Block Type'),
     '#default_value' => isset($param['bundle']) ? $param['bundle'] : '',
     '#attributes' => [
      'name' => 'bundle'
      ],
     '#options' => $options
   ];
    $form['id'] = array(
      '#type' => 'textfield',
      '#default_value' => isset($param['id']) ? $param['id'] : '',
      '#attributes' => [
        'name' => 'id'
      ],
      '#description' => 'Leave empty to get the last block content by selected type'
    );


    $block_id = 0 ;
    $entity_name= 'block_content';
    if(isset($param['entity_name'])){
      $entity_name = $param['entity_name'] ; 
    }
    if(isset($param['bundle'])){
      $block_id = $this->getLastBlockContent($param['bundle'],$entity_name);    
    }
    if(isset($param['id']) && $param['id'] != ''){
      $block_id = $param['id']; 
    } 
    if($block_id != 0){
      $block = \Drupal::entityTypeManager()->getStorage($entity_name)->load($block_id);
      $render = \Drupal::entityTypeManager()->getViewBuilder($entity_name)->view($block);
      $form['block_item'] = [
        '#theme' => 'block',
        '#weight' => 999,
        'content' => $render,
         '#base_plugin_id' => 'preview_display',
        '#cache' => [
          'max-age' => 0,
        ],
      ];
      $bundle = $block->bundle();
      $activeThemeName = \Drupal::service('theme.manager')->getActiveTheme();
      $theme = $activeThemeName->getName();
      $suggestion_1 = 'template.block--'.$theme.'-'.$bundle.'.html.twig' ;
      $suggestion_1 = str_replace('_', '-', $suggestion_1);

      $url_template = '/admin/config/template/edit?config='.$suggestion_1 ;

      $url = Url::fromRoute('entity.block_content.canonical', ['block_content' => $block_id]);
      $url = $url->toString(TRUE);
      $url_str = $url->getGeneratedUrl();
  
      $url_type = '/admin/structure/block/block-content/manage/'.$bundle.'/fields' ;
      $form['help_text'] = [
        '#markup' => '<div class="btn-preview"><a href="'. $url_str.'" target="_blank"> Edit block </a><a href="'. $url_type.'" target="_blank"> Edit block Type </a><a href="'. $url_template.'" target="_blank"> Edit block Template</a></div>'
      ];
    }else{
      $render = 'Not Exist Content';
      $form['block_item'] = [
        '#theme' => 'block',
        '#weight' => 999,
        'content' => $render,
        '#cache' => [
          'max-age' => 0,
        ],
      ];
      $form['block_item']['#prefix'] = '<div id="preview-empty-display">';
      $form['block_item']['#suffix'] = '</div>';
      if(isset($param['bundle'])){

        $url = Url::fromRoute('block_content.add_form', ['block_content_type' => $param['bundle']]);
        $url = $url->toString(TRUE);
        $url_str = $url->getGeneratedUrl();
        $form['help_text'] = [
          '#markup' => '<div class="btn-preview"><a href="'. $url_str.'" target="_blank"> Add New block </a></div>'
        ];
      }
    
    }
   

    $form['#cache']['max-age'] = 0;
    $form['#attached']['library'][] = 'preview_entity/preview_entity';
    $form['#prefix'] = '<div id="form-preview-entity-display">';
    $form['#suffix'] = '</div>';
    $form['action'] = array(
      '#type' => 'submit',
      '#value' => t('Load Entity')   
    );
    $form['action']['#prefix'] = '<div class="form-preview-entity-action">';
    $form['action']['#suffix'] = '</div>';
    return $form ;
  }
  public function customCallback(array &$form, FormStateInterface $form_state) {
    $list = $form_state->get('list');
    $form['fieldset']['list']['#description'] = $list;
    return $form['fieldset'];
  }
  public function getLastBlockContent($bundle,$type = 'block_content'){
    $block_id = 0 ;

    $query = \Drupal::entityQuery($type);
    $query->condition('type',$bundle );  
    $query->sort('id','DESC');
    $query->range(0,1);
    $res = $query->execute();
    if(!empty($res)){
      $block_id = end($res);
    }  
    return  $block_id ;
  }
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->disableRedirect();
  }
  public function get_parameter($param = null)
  {
      $method = \Drupal::request()->getMethod();
      if ($param == null) {
          if ($method == "GET") {
              return \Drupal::request()->query->all();
          } elseif ($method == "POST") {
              return \Drupal::request()->request->all();
          } else {
              return null;
          }
      } else {
          if ($method == "GET") {
              return \Drupal::request()->query->get($param);
          } elseif ($method == "POST") {
              return \Drupal::request()->request->get($param);
          } else {
              return null;
          }
      }
  }

}
